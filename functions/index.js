const functions = require('firebase-functions');
const request = require('request-promise');
require('dotenv').config()

const LINE_MESSAGING_API = 'https://api.line.me/v2/bot/message';
const LINE_HEADER = {
  'Content-Type': 'application/json',
  'Authorization': 'Bearer '+process.env.LINE_BOT_TOKEN
};


exports.notification = functions.https.onRequest((req, res) => {
    if (req.body.events[0].message.type !== 'text') {
      return;
    }
    reply(req.body);
});

const reply = (bodyResponse) => {
    var option ={
      method: `POST`,
      uri: `${LINE_MESSAGING_API}/reply`,
      headers: LINE_HEADER,
      body: JSON.stringify({
        replyToken: bodyResponse.events[0].replyToken,
        messages: [
          {
            type: `text`,
            text: 'Thank you for question ^__^.'
          }
        ]
      }),
      timeout:10000
    }
    return request(option).catch((err) => {
      if(err.message === 'Error: ETIMEDOUT')
      {
          var options = {
            'method': 'POST',
            'url': 'https://notify-api.line.me/api/notify',
            'headers': {
              'Content-Type': ['application/x-www-form-urlencoded'],
              'Authorization': 'Bearer '+process.env.LINE_NOTIFIY
            },
            form: {
              'message': '[Problem] '+bodyResponse.events[0].message.text+' answer was delayed.!!!'
            }
          };
          request(options, function (error, response) { 
            if (error) throw new Error(error);
            console.log(response.body);
          });
      }
    });
    
    
};
